﻿using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Challange;
using BattleNetAPI.Model.Infrastructure;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class ChallengesTests
    {
        private  WowBattleNetApiClient _battleNetApiClient;
        private  Challenges challenges;

        [SetUp]
        public  void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.EU, Locale.ru_RU, apiKey);
        }

        [Test]
        public void Get_Challenges_From_Skullcrusher()
        {
            challenges = _battleNetApiClient.GetChallengesAsync("Ясеневый лес").Result;
            Assert.AreEqual(26, challenges.Challenge.Count());
            Assert.AreEqual("Аукиндон", challenges.Challenge.First().Map.Name);
        }
    }
}