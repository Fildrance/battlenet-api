﻿using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Infrastructure;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class CharacterTests
    {

        private WowBattleNetApiClient _battleNetApiClient;

        [SetUp]
        public  void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.US, Locale.en_US, apiKey);
        }

        

        [Test]
        public void Get_Complex_Character_Briandek_From_Skullcrusher()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetEverything).Result;

            Assert.IsNotNull(briandek.Guild);
            Assert.IsNotNull(briandek.Stats);
            Assert.IsNotNull(briandek.Talents);
            Assert.IsNotNull(briandek.Items);
            Assert.IsNotNull(briandek.Reputation);
            Assert.IsNotNull(briandek.Titles);
            Assert.IsNotNull(briandek.Professions);
            Assert.IsNotNull(briandek.Appearance);
            Assert.IsNotNull(briandek.PetSlots);
            Assert.IsNotNull(briandek.Mounts);
            Assert.IsNotNull(briandek.Pets);
            Assert.IsNotNull(briandek.Achievements);
            Assert.IsNotNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));

            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);

            // TODO: glyphs changed in 5.0.4 change test accordingly
            //Assert.IsTrue(briandek.Talents.Where(t => t.Selected).FirstOrDefault().Name.Equals("protection", StringComparison.InvariantCultureIgnoreCase));
            //Assert.IsTrue(briandek.Talents.ElementAt(1).Glyphs.Prime.ElementAt(0).Name.Equals("Glyph of Revenge", StringComparison.InvariantCultureIgnoreCase));

            Assert.AreEqual(0, briandek.Appearance.HairVariation);
            Assert.IsTrue(briandek.Mounts.NumCollected > 1);
        }


        [Test]
        public void Get_Complex_Character_Talasi_From_Skullcrusher()
        {
            Character talasi = _battleNetApiClient.GetCharacterAsync("skullcrusher", "Zaku", CharacterOptions.GetEverything).Result;

            Assert.IsNotNull(talasi.Guild);
            Assert.IsNotNull(talasi.Stats);
            Assert.IsNotNull(talasi.Talents);
            Assert.IsNotNull(talasi.Items);
            Assert.IsNotNull(talasi.Reputation);
            Assert.IsNotNull(talasi.Titles);
            Assert.IsNotNull(talasi.Professions);
            Assert.IsNotNull(talasi.Appearance);
            Assert.IsNotNull(talasi.PetSlots);
            Assert.IsNotNull(talasi.HunterPets);
            Assert.IsNotNull(talasi.Mounts);
            Assert.IsNotNull(talasi.Pets);
            Assert.IsNotNull(talasi.Achievements);
            Assert.IsNotNull(talasi.Progression);

            Assert.IsTrue(talasi.HunterPets.Any());
            Assert.IsTrue(talasi.Name.Equals("Zaku", StringComparison.InvariantCultureIgnoreCase));

            Assert.AreEqual(CharacterClass.HUNTER, talasi.@Class);
            Assert.AreEqual(CharacterRace.DWARF, talasi.Race);
            Assert.AreEqual(CharacterGender.MALE, talasi.Gender);

            Assert.IsTrue(talasi.Mounts.NumCollected > 1);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek").Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(100, briandek.Level);
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithAchievements()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetAchievements).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNotNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithAppearance()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetAppearance).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNotNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithGuild()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetGuild).Result;

            Assert.IsNotNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithItems()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetItems).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNotNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);

            Assert.IsTrue(briandek.Items.Head.ItemLevel >= 670);
            Assert.IsTrue(briandek.Items.Head.Armor>= 286);
            //Assert.AreEqual(briandek.Items.Head.TooltipParams.ItemUpgrade.Current, 0);
            //Assert.AreEqual(briandek.Items.Finger2.TooltipParams.ItemUpgrade.Total, 4);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithMounts()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetMounts).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNotNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
            Assert.IsTrue(briandek.Mounts.NumCollected > 1);
            Assert.IsTrue(briandek.Mounts.NumNotCollected > 1);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithPetSlots()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetPetSlots).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNotNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
            Assert.IsTrue(briandek.PetSlots.Any());
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithProfessions()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetProfessions).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNotNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithProgression()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetProgression).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNotNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithReputations()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetReputation).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNotNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithStats()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetStats).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNotNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithTalents()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetTalents).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNotNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Briandek_From_Skullcrusher_WithTitles()
        {
            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetTitles).Result;

            Assert.IsNull(briandek.Guild);
            Assert.IsNull(briandek.Stats);
            Assert.IsNull(briandek.Talents);
            Assert.IsNull(briandek.Items);
            Assert.IsNull(briandek.Reputation);
            Assert.IsNotNull(briandek.Titles);
            Assert.IsNull(briandek.Professions);
            Assert.IsNull(briandek.Appearance);
            Assert.IsNull(briandek.PetSlots);
            Assert.IsNull(briandek.Mounts);
            Assert.IsNull(briandek.Pets);
            Assert.IsNull(briandek.Achievements);
            Assert.IsNull(briandek.Progression);

            Assert.IsTrue(briandek.Name.Equals("briandek", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.WARRIOR, briandek.@Class);
            Assert.AreEqual(CharacterRace.HUMAN, briandek.Race);
            Assert.AreEqual(CharacterGender.MALE, briandek.Gender);
        }

        [Test]
        public void Get_Simple_Character_Lukenukem_From_Illidan_WithPets()
        {
            Character lukenukem = _battleNetApiClient.GetCharacterAsync("illidan", "lukenukem", CharacterOptions.GetPets).Result;

            Assert.IsNull(lukenukem.Guild);
            Assert.IsNull(lukenukem.Stats);
            Assert.IsNull(lukenukem.Talents);
            Assert.IsNull(lukenukem.Items);
            Assert.IsNull(lukenukem.Reputation);
            Assert.IsNull(lukenukem.Titles);
            Assert.IsNull(lukenukem.Professions);
            Assert.IsNull(lukenukem.Appearance);
            Assert.IsNull(lukenukem.PetSlots);
            Assert.IsNull(lukenukem.Mounts);
            Assert.IsNotNull(lukenukem.Pets);
            Assert.IsNull(lukenukem.Achievements);
            Assert.IsNull(lukenukem.Progression);

            Assert.IsTrue(lukenukem.Name.Equals("lukenukem", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(lukenukem.Pets.NumNotCollected > 0);
        }

        [Test]
        public void Get_Simple_Character_Shirokuma_From_Skullcrusher_WithHunterPets()
        {
            Character shirokuma = _battleNetApiClient.GetCharacterAsync("skullcrusher", "shirokuma", CharacterOptions.GetHunterPets).Result;

            Assert.IsNull(shirokuma.Guild);
            Assert.IsNull(shirokuma.Stats);
            Assert.IsNull(shirokuma.Talents);
            Assert.IsNull(shirokuma.Items);
            Assert.IsNull(shirokuma.Reputation);
            Assert.IsNull(shirokuma.Titles);
            Assert.IsNull(shirokuma.Professions);
            Assert.IsNull(shirokuma.Appearance);
            Assert.IsNull(shirokuma.PetSlots);
            Assert.IsNull(shirokuma.Mounts);
            Assert.IsNotNull(shirokuma.HunterPets);
            Assert.IsNull(shirokuma.Pets);
            Assert.IsNull(shirokuma.Achievements);
            Assert.IsNull(shirokuma.Progression);

            Assert.IsTrue(shirokuma.Name.Equals("shirokuma", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(CharacterClass.HUNTER, shirokuma.@Class);
            Assert.AreEqual(CharacterRace.DWARF, shirokuma.Race);
            Assert.AreEqual(CharacterGender.MALE, shirokuma.Gender);
            Assert.IsTrue(shirokuma.HunterPets.Count() > 1);
        }
    }
}