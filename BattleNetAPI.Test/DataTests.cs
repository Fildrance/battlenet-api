﻿using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Achivement;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Guild;
using BattleNetAPI.Model.Infrastructure;
using BattleNetAPI.Model.Item;
using BattleNetAPI.Utilities;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class DataTests
    {
        private  WowBattleNetApiClient _battleNetApiClient;

        [SetUp]
        public void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.US, Locale.en_US, apiKey);
        }

        [Test]
        public void Get_BattleGroups_Test()
        {
            IEnumerable<BattlegroupInfo> battleGroups = _battleNetApiClient.GetBattlegroupsDataAsync().Result.ToArray();

            Assert.AreEqual(9, battleGroups.Count());
            Assert.IsTrue(battleGroups.Any(r => r.Name == "Cyclone"));
        }

        [Test]
        public void Get_Character_Races_Data()
        {
            IEnumerable<CharacterRaceInfo> races = _battleNetApiClient.GetCharacterRacesAsync().Result.ToArray();

            Assert.AreEqual(15, races.Count());
            Assert.IsTrue(races.Any(r => r.Name == "Human" || r.Name == "Night Elf"));
        }

        [Test]
        public void Get_Character_Achievements_Data()
        {
            IEnumerable<AchievementList> characterAchievements = _battleNetApiClient.GetAchievementsAsync().Result.ToArray();

            Assert.AreEqual(15, characterAchievements.Count());
            AchievementList achievementList = characterAchievements.First(a => a.Id == 92);
            AchievementInfo gotMyMindOnMyMoneyAchievement = achievementList.Achievements.First(a => a.Id == 1181);
            Assert.AreEqual("Loot 25,000 gold", gotMyMindOnMyMoneyAchievement.Criteria.ElementAt(0).Description);
        }

        [Test]
        public void Get_Character_Classes_Data()
        {
            IEnumerable<CharacterClassInfo> classes = _battleNetApiClient.GetCharacterClassesAsync().Result.ToArray();

            Assert.IsTrue(classes.Count() == 12);
            Assert.IsTrue(classes.Any(r => r.Name == "Warrior" || r.Name == "Death Knight"));
        }

        [Test]
        public void Get_Guild_Achievements_Data()
        {
            IEnumerable<AchievementList> guildAchievementsList = _battleNetApiClient.GetGuildAchievementsAsync().Result;
            Assert.AreEqual(7, guildAchievementsList.Count());
        }

        [Test]
        public void Get_Guild_Rewards_Data()
        {
            IEnumerable<GuildRewardInfo> rewards = _battleNetApiClient.GetGuildRewardsAsync().Result.ToArray();
            Assert.AreEqual(64, rewards.Count());
            Assert.IsTrue(rewards.Any(r => r.Achievement != null));
        }


        [Test]
        public void Get_Guild_Perks_Data()
        {
            IEnumerable<GuildPerkInfo> perks = _battleNetApiClient.GetGuildPerksAsync().Result.ToArray();
            Assert.AreEqual(5, perks.Count());
            Assert.IsTrue(perks.Any(r => r.Spell != null));
        }

        [Test]
        public void Get_Realms_From_Json_String()
        {
            IEnumerable<Realm> realms1 = _battleNetApiClient.GetRealmsAsync().Result;
            IEnumerable<Realm> realms2 = JsonUtility.GetFromJson<RealmsData>(TestUtility.TestUtility.GetStringResource("RealmTest")).Realms;
            IEnumerable<Realm> realms3 = realms1.Intersect(realms2);
            Assert.AreEqual(0, realms3.Count());
        }

        [Test]
        public void Get_Character_From_Json_String()
        {

            Character briandek = _battleNetApiClient.GetCharacterAsync("Spirestone", "briandek", CharacterOptions.GetEverything).Result;
            var briandekFromJsonString = JsonUtility.GetFromJson<Character>(TestUtility.TestUtility.GetStringResource("CharacterTest"));
            Assert.AreEqual(0, briandek.CompareTo(briandekFromJsonString));
        }

        [Test]
        public void Get_Item_Classes()
        {
            IEnumerable<ItemClassInfo> itemClasses = _battleNetApiClient.GetItemClassesAsync().Result;
            Assert.AreEqual(16, itemClasses.Count());
        }
    }
}