﻿using System.Configuration;
using System.Linq;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Guild;
using BattleNetAPI.Model.Infrastructure;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [TestFixture]
    public class DeamonHuntersTest
    {
        private IWoWBattleNetApiClient _battleNetApiClient;

        [SetUp]
        public void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.EU, Locale.ru_RU, apiKey);
        }

        [Test]
        public async void GetDeamonHunter()
        {
            var dh = await _battleNetApiClient.GetCharacterAsync("howling-fjord", "Нарзекашашлы", CharacterOptions.GetEverything);
            Assert.NotNull(dh);
            Assert.AreEqual(CharacterClass.DEMON_HUNTER, dh.Class);
        }

        [Test]
        public async void GetClassInfo_deamonhunterResourceExist()
        {
            var ci = await _battleNetApiClient.GetCharacterClassesAsync();
            Assert.True(ci.Any(x=>x.PowerType == CharacterPowerType.FURY));
        }

        [Test]
        public async void GetGuildAsync_getDeamonHunterMember()
        {
            var guild =
                await _battleNetApiClient.GetGuildAsync("howling-fjord", "Грань будущего", GuildOptions.GetMembers);

            var dh = guild.Members.SingleOrDefault(x => x.Character.Name == "Нарзекашашлы");
            Assert.NotNull(dh);
            Assert.AreEqual(CharacterClass.DEMON_HUNTER, dh.Character.Class);
        }
    }
}