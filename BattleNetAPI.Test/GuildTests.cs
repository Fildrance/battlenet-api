﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Guild;
using BattleNetAPI.Model.Infrastructure;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class GuildTests
    {
        [SetUp]
        public void Setup()
        {
            _apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.US, Locale.en_US, _apiKey);
            _guild = _battleNetApiClient.GetGuildAsync("skullcrusher", "immortality", GuildOptions.GetEverything).Result;
        }

        private WowBattleNetApiClient _battleNetApiClient;
        private Guild _guild;
        private string _apiKey;


        [Test]
        public void Get_Guild_With_Base_Method_Call()
        {
            Guild guild = _battleNetApiClient.GetGuildAsync("skullcrusher", "immortality").Result;

            Assert.IsNull(guild.Members);
            Assert.IsNull(guild.Achievements);

            Assert.IsTrue(guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(UnitSide.ALLIANCE, guild.Side);
        }

        [Test]
        public void Get_Guild_With_Connected_Realms()
        {
            var explorer2 = new WowBattleNetApiClient(Region.EU, Locale.en_GB, _apiKey);
            Guild guild2 = explorer2.GetGuildAsync("darksorrow", "mentality", GuildOptions.GetMembers).Result;
            List<GuildMember> guildMembers =
                guild2.Members.Where(
                    x => x.Character.Name.Equals("Dardray", StringComparison.CurrentCultureIgnoreCase)).ToList();

            //Assert.AreEqual(2, guildMembers.Count); //shure it still works! it's just hard to repro :( lot of work...
            Assert.AreEqual(1, guildMembers.Count(x => !x.Character.Realm.Equals(x.Character.GuildRealm)));
        }

        [Test]
        public void Get_Guild_With_Only_Achievements()
        {
            Guild guild =
                _battleNetApiClient.GetGuildAsync("skullcrusher", "immortality", GuildOptions.GetAchievements).Result;


            Assert.IsNull(guild.Members);
            Assert.IsNotNull(guild.Achievements);

            Assert.IsTrue(guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(UnitSide.ALLIANCE, guild.Side);
        }

        [Test]
        public void Get_Guild_With_Only_Members()
        {
            Guild guild =
                _battleNetApiClient.GetGuildAsync("skullcrusher", "immortality", GuildOptions.GetMembers).Result;

            Assert.IsNotNull(guild.Members);
            Assert.IsNull(guild.Achievements);

            Assert.IsTrue(guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(UnitSide.ALLIANCE, guild.Side);
        }

        [Test]
        public void Get_Guild_With_Only_No_Options()
        {
            Guild guild = _battleNetApiClient.GetGuildAsync("skullcrusher", "immortality", GuildOptions.None).Result;

            Assert.IsNull(guild.Members);
            Assert.IsNull(guild.Achievements);

            Assert.IsTrue(guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(UnitSide.ALLIANCE, guild.Side);
        }

        [Test]
        public void Get_Simple_Guild_Immortality_From_Skullcrusher()
        {
            Assert.IsTrue(_guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.AreEqual(UnitSide.ALLIANCE, _guild.Side);
            Assert.IsTrue(_guild.Members.Any());
        }

        [Test]
        public void Get_Valid_Human_Member_From_Immortality_Guild()
        {
            GuildMember guildMember =
                _guild.Members.FirstOrDefault(
                    m => m.Character.Name.Equals("Zaku", StringComparison.InvariantCultureIgnoreCase));
            Assert.NotNull(guildMember);
            Assert.IsTrue(guildMember.Character.Name.Equals("Zaku", StringComparison.InvariantCultureIgnoreCase));

            Assert.AreEqual(100, guildMember.Character.Level);
            Assert.AreEqual(CharacterClass.HUNTER, guildMember.Character.@Class);
            Assert.AreEqual(CharacterRace.DWARF, guildMember.Character.Race);
            Assert.AreEqual(CharacterGender.MALE, guildMember.Character.Gender);

            Assert.IsTrue(guildMember.Character.AchievementPoints >= 11750);
        }


        [Test]
        public void Get_Valid_Member_From_Another_Guild()
        {
            Guild guild = _battleNetApiClient.GetGuildAsync("laughing skull", "deus vox",
                GuildOptions.GetMembers | GuildOptions.GetAchievements).Result;


            Assert.IsNotNull(guild.Members);
            Assert.IsNotNull(guild.AchievementPoints);

            Assert.IsTrue(guild.Name.Equals("deus vox", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(guild.Realm.Equals("laughing skull", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(guild.Members.Any());

            Assert.AreEqual(UnitSide.ALLIANCE, guild.Side);
        }

        [Test]
        public void Get_Valid_Member_From_Horde_Guild()
        {
            Guild guild = _battleNetApiClient.GetGuildAsync("skullcrusher", "rage", GuildOptions.GetMembers).Result;

            Assert.IsNotNull(guild.Members);
            Assert.IsNull(guild.Achievements);

            Assert.IsTrue(guild.Name.Equals("rage", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsTrue(guild.Realm.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));

            Assert.IsTrue(guild.Members.Any());

            Assert.IsTrue(guild.Side == UnitSide.HORDE);
        }

        [Test]
        public void Get_Valid_Night_Elf_Member_From_Immortality_Guild()
        {
            GuildMember guildMember =
                _guild.Members.FirstOrDefault(
                    m => m.Character.Name.Equals("Wru", StringComparison.InvariantCultureIgnoreCase));
            Assert.NotNull(guildMember);
            Assert.IsTrue(guildMember.Character.Name.Equals("Wru", StringComparison.InvariantCultureIgnoreCase));

            Assert.AreEqual(100, guildMember.Character.Level);
            Assert.AreEqual(CharacterClass.SHAMAN, guildMember.Character.@Class);
            Assert.AreEqual(CharacterRace.DRAENEI, guildMember.Character.Race);
            Assert.AreEqual(CharacterGender.MALE, guildMember.Character.Gender);
        }
    }
}