﻿using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Infrastructure;
using BattleNetAPI.Model.Item;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [TestFixture]
    [ExcludeFromCodeCoverage]
    public class ItemTests
    {
        private  WowBattleNetApiClient _battleNetApiClient;
        [SetUp]
        public  void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.US, Locale.en_US, apiKey);
        }


        [Test]
        public void Get_Sample_Gem_52210()
        {
            Item sampleItem = _battleNetApiClient.GetItemAsync(52210).Result;

            Assert.AreEqual("+8 Parry and +4 Stamina", sampleItem.GemInfo.Bonus.Name);
            Assert.AreEqual("PURPLE", sampleItem.GemInfo.Type.Color);
            Assert.AreEqual(sampleItem.Id, sampleItem.GemInfo.Bonus.SourceItemId);
            Assert.AreEqual(0, sampleItem.GemInfo.Bonus.RequiredSkillRank);
            Assert.AreEqual(0, sampleItem.GemInfo.Bonus.MinLevel);
            Assert.AreEqual(290, sampleItem.GemInfo.Bonus.ItemLevel);
        }

        [Test]
        public void Get_Sample_Item_17182()
        {
            Item sampleItem = _battleNetApiClient.GetItemAsync(17182).Result;

            Assert.AreEqual("Sulfuras, Hand of Ragnaros", sampleItem.Name);
            Assert.AreEqual("", sampleItem.Description);
            Assert.AreEqual("inv_hammer_unique_sulfuras", sampleItem.Icon);
            Assert.AreEqual(1, sampleItem.Stackable);
            Assert.AreEqual(1, sampleItem.ItemBind);

            Assert.AreEqual("CREATED_BY_SPELL", sampleItem.ItemSource.SourceType);

            Assert.AreEqual(3.7, sampleItem.WeaponInfo.WeaponSpeed);

            Assert.AreEqual(8, sampleItem.BonusStats.ElementAt(2).Amount);
            Assert.AreEqual(
                "Hurls a fiery ball that causes ^8.0452 Fire damage and an additional ^1.9910 damage over 10 sec.",
                sampleItem.ItemSpells.First().Spell.Description);
        }

        [Test]
        public void Get_Sample_Item_38268()
        {
            Item sampleItem = _battleNetApiClient.GetItemAsync(38268).Result;

            Assert.AreEqual("Spare Hand", sampleItem.Name);
            Assert.AreEqual("Give to a Friend", sampleItem.Description);
            Assert.AreEqual("inv_gauntlets_09", sampleItem.Icon);
            Assert.AreEqual(1, sampleItem.Stackable);
            Assert.AreEqual(0, sampleItem.ItemBind);
            Assert.AreEqual("WORLD_DROP", sampleItem.ItemSource.SourceType);
            Assert.AreEqual(1, sampleItem.WeaponInfo.Damage.MaxDamage);
        }


        [Test]
        public void Get_Sample_Item_39564()
        {
            Item sampleItem = _battleNetApiClient.GetItemAsync(39564).Result;

            Assert.AreEqual(@"Heroes' Bonescythe Legplates", sampleItem.Name);
            Assert.AreEqual("", sampleItem.Description);
            Assert.AreEqual("inv_pants_mail_15", sampleItem.Icon);
            Assert.AreEqual(1, sampleItem.Stackable);
            Assert.AreEqual(1, sampleItem.ItemBind);
            Assert.AreEqual("VENDOR", sampleItem.ItemSource.SourceType);

            Assert.AreEqual(null, sampleItem.WeaponInfo);

            Assert.AreEqual(4, sampleItem.AllowableClasses.First());
            Assert.AreEqual(25, sampleItem.BonusStats.ElementAt(2).Amount);

            Assert.AreEqual("PRISMATIC", sampleItem.SocketInfo.Sockets.First().Type);
        }
    }
}