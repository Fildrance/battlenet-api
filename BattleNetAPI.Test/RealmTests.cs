﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BattleNetAPI.Model.Infrastructure;
using NUnit.Framework;

namespace BattleNetAPI.Test
{
    [ExcludeFromCodeCoverage]
    [TestFixture]
    public class RealmTests
    {
        private WowBattleNetApiClient _battleNetApiClient;

        [SetUp]
        public  void Setup()
        {
            var apiKey = ConfigurationManager.AppSettings["WoWApiKey"];
            _battleNetApiClient = new WowBattleNetApiClient(Region.EU, Locale.en_GB, apiKey);
        }

        [Test]
        public void GetAll_US_Realms_Returns_All_Realms()
        {
            IEnumerable<Realm> realmList = _battleNetApiClient.GetRealmsAsync().Result;
            Assert.IsTrue(realmList.Any());
        }

        [Test]
        public void Get_All_Realms_By_Population_Returns_Realms_That_Have_Low_Population()
        {
            IEnumerable<Realm> realmList = _battleNetApiClient.GetRealmsAsync().Result.Where(r => r.population == "low").ToArray();
            bool allCollectedRealmsHaveLowPopulation = realmList.Any() && realmList.All(r => r.population == "low");
            Assert.IsTrue(allCollectedRealmsHaveLowPopulation);
        }

        [Test]
        public void Get_All_Realms_By_Status_Returns_Realms_That_Are_Up()
        {
            IEnumerable<Realm> realmList = _battleNetApiClient.GetRealmsAsync().Result.Where(r => r.Status).ToArray();
            //All servers being down is likely(maintenance) and will cause test to fail
            bool allCollectedRealmsAreUp = realmList.Any() && realmList.All(r => r.Status);
            Assert.IsTrue(allCollectedRealmsAreUp);
        }

        [Test]
        public void Get_All_Realms_By_Type_Returns_Pvp_Realms()
        {
            IEnumerable<Realm> realms = _battleNetApiClient.GetRealmsAsync().Result.Where(r => r.Type == RealmType.PVP).ToArray();
            bool allCollectedRealmsArePvp = realms.Any() && realms.All(r => r.Type == RealmType.PVP);
            Assert.IsTrue(allCollectedRealmsArePvp);
        }

        [Test]
        public void Get_Valid_US_Realm_Returns_Unique_Realm()
        {
            Realm realm =
                _battleNetApiClient.GetRealmsAsync().Result
                    .FirstOrDefault(r => r.Name.Equals("skullcrusher", StringComparison.InvariantCultureIgnoreCase));
            Assert.IsNotNull(realm);
            Assert.IsTrue(realm.Name == "Skullcrusher");
            Assert.IsTrue(realm.Type == RealmType.PVP);
            Assert.IsTrue(realm.Slug == "skullcrusher");
        }

    }
}