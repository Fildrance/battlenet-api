﻿using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace BattleNetAPI.Test.TestUtility
{
    [ExcludeFromCodeCoverage]
    public static class TestUtility
    {
        public static string GetStringResource(string resourceName)
        {
            string resource;
            using (
                var streamReader =
                    new StreamReader(File.Open(string.Format("Resources\\{0}.txt", resourceName), FileMode.Open)))
            {
                resource = streamReader.ReadToEnd();
            }
            return resource;
        }
    }
}