﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BattleNetAPI.Model.Achivement;
using BattleNetAPI.Model.Challange;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Guild;
using BattleNetAPI.Model.Infrastructure;
using BattleNetAPI.Model.Item;

namespace BattleNetAPI
{
    public interface IWoWBattleNetApiClient
    {
        Task<Character> GetCharacterAsync(string realm, string name);
        Task<Character> GetCharacterAsync(string realm, string name, CharacterOptions characterOptions);

        Task<Guild> GetGuildAsync(string realm, string name);
        Task<Guild> GetGuildAsync(string realm, string name, GuildOptions guildOptions);

        Task<AchievementInfo> GetAchievementAsync(int id);

        Task<IEnumerable<AchievementList>> GetAchievementsAsync();
        Task<IEnumerable<AchievementList>> GetGuildAchievementsAsync();

        Task<IEnumerable<BattlegroupInfo>> GetBattlegroupsDataAsync();

        Task<IEnumerable<ItemClassInfo>> GetItemClassesAsync();

        Task<IEnumerable<Realm>> GetRealmsAsync();

        Task<Item> GetItemAsync(int id);

        Task<IEnumerable<CharacterRaceInfo>> GetCharacterRacesAsync();

        Task<IEnumerable<CharacterClassInfo>> GetCharacterClassesAsync();

        Task<IEnumerable<GuildRewardInfo>> GetGuildRewardsAsync();

        Task<IEnumerable<GuildPerkInfo>> GetGuildPerksAsync();

        Task<Challenges> GetChallengesAsync(string realm);
    }
}