﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Auction
{
    [DataContract]
    public class AuctionFile
    {
        [DataMember(Name = "url")]
        public string URL { get; set; }
        [DataMember(Name = "lastModified")]
        public long LastModified { get; set; }
    }
}
