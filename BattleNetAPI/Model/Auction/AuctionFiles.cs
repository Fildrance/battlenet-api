﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Auction
{
    [DataContract]
    public class AuctionFiles
    {
        [DataMember(Name = "files")]
        public IEnumerable<AuctionFile> Files { get; set; }
    }
}
