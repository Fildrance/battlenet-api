﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Auction
{
    [DataContract]
    public class Auctions
    {
        [DataMember(Name = "horde")]
        public AuctionHouseSide Horde { get; set; }
        [DataMember(Name = "alliance")]
        public AuctionHouseSide Alliance { get; set; }
        [DataMember(Name = "neutral")]
        public AuctionHouseSide Neutral { get; set; }
    }
}
