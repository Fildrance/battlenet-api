﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Challange
{
    [DataContract]
    public class Challenge
    {
        [DataMember(Name = "groups")]
        public IEnumerable<ChallengeGroup> Groups { get; set; }

        [DataMember(Name = "map")]
        public ChallengeMap Map { get; set; }
    }
}
