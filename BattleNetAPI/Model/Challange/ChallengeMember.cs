﻿using System.Runtime.Serialization;
using BattleNetAPI.Model.Character;

namespace BattleNetAPI.Model.Challange
{
    [DataContract]
    public class ChallengeMember
    {
        [DataMember(Name = "character")]
        public ChallengeMemberCharacter Character { get; set; }

        [DataMember(Name = "spec")]
        public CharacterTalentSpec Spec { get; set; }
    }
}
