﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Character
{
    [DataContract]
    public class CharacterClassesData
    {
        [DataMember(Name = "classes")]
        public IEnumerable<CharacterClassInfo> Classes { get; set; }
    }
}
