﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Character
{
    [DataContract]
    public class CharacterMounts
    {
        [DataMember(Name = "numCollected")]
        public int NumCollected { get; set; }

        [DataMember(Name = "numNotCollected")]
        public int NumNotCollected { get; set; }
        
        [DataMember(Name = "collected")]
        public IEnumerable<CharacterMount> Collected { get; set; }        
    }
}
