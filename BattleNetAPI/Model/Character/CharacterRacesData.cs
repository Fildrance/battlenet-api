﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Character
{
    [DataContract]
    public class CharacterRacesData
    {
        [DataMember(Name="races")]
        public IEnumerable<CharacterRaceInfo> Races { get; set; }
    }
}
