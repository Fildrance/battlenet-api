﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Character
{
    [DataContract]
    public class CharacterRatedBattlegrounds
    {
        [DataMember(Name = "personalRating")]
        public int PersonalRating { get; set; }

        [DataMember(Name = "battlegrounds")]
        public IEnumerable<CharacterRatedBattleground> Battlegrounds { get; set; }
    }
}
