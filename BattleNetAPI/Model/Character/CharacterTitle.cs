﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Character
{
    [DataContract]
	public class CharacterTitle
	{
        [DataMember(Name="id")]
		public int Id { get; set; }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }
	}
}
