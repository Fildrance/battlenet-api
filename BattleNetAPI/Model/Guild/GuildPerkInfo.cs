﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Guild
{
    [DataContract]
    public class GuildPerkInfo
    {
        [DataMember(Name="guildLevel")]
        public int GuildLevel { get; set; }

        [DataMember(Name = "spell")]
        public GuildPerkSpellInfo Spell { get; set; }

    }
}
