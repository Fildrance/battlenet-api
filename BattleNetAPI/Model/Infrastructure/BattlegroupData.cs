﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Infrastructure
{
    [DataContract]
    public class BattlegroupData
    {
        [DataMember(Name = "battlegroups")]
        public IEnumerable<BattlegroupInfo> Battlegroups { get; set; }
    }
}
