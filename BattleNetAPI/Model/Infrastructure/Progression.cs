﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Infrastructure
{
    [DataContract]
	public class Progression
	{
        [DataMember(Name="raids")]
		public IEnumerable<Raid> Raids { get; set; }
	}
}
