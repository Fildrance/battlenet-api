﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Infrastructure
{
    [DataContract]
    public class RealmsData
    {
        [DataMember(Name = "realms")]
        public IEnumerable<Realm> Realms { get; set; }
    }
}
