﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Item
{
    [DataContract]
    public class ItemBonusStat
    {
        [DataMember(Name = "stat")]
        public int Stat { get; set; }

        [DataMember(Name = "amount")]
        public int Amount { get; set; }

        [DataMember(Name = "reforged")]
        public bool Reforged { get; set; }
    }
}
