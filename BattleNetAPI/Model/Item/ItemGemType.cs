﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Item
{
    [DataContract]
    public class ItemGemType
    {
        [DataMember(Name = "type")]
        public string Color { get; set; }
    }
}
