﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Item
{
    [DataContract]
    public class ItemSocket
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }
    }
}
