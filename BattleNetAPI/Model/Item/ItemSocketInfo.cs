﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Item
{
    [DataContract]
    public class ItemSocketInfo
    {
        [DataMember(Name="sockets")]
        public IEnumerable<ItemSocket> Sockets { get; set; }  
    }
}
