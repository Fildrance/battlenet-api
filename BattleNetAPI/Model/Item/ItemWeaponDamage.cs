﻿using System.Runtime.Serialization;

namespace BattleNetAPI.Model.Item
{
    [DataContract]
    public class ItemWeaponDamage
    {
        [DataMember(Name="min")]
        public int MinDamage { get; set; }

        [DataMember(Name = "max")]
        public int MaxDamage { get; set; }
        
    }
}
