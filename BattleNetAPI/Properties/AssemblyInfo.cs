﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("BattleNetAPI")]
[assembly: AssemblyDescription("BattleNet API project for .NET")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Lemon Inc")]
[assembly: AssemblyProduct("BattleNetAPI")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]

[assembly: AssemblyVersion("1.0.14")]
[assembly: AssemblyFileVersion("1.0.14")]
[assembly: InternalsVisibleTo("BattleNetAPI.Test")]