﻿using System.Net.Http;
using System.Threading.Tasks;

namespace BattleNetAPI.Utilities
{
    internal static class HttpUtility
    {
        private static readonly HttpClient client = new HttpClient();

        internal static Task<string> GetResponceAsync(string url)
        {
            return client.GetStringAsync(url);
        }
    }
}