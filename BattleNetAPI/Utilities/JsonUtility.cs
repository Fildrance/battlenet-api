﻿using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BattleNetAPI.Utilities
{
    internal static class JsonUtility
    {
        internal static T GetFromJson<T>(string jsonString)
        {
            var contractResolver = new PrivateSetterContractResolver();
            var settings = new JsonSerializerSettings {ContractResolver = contractResolver};

            return JsonConvert.DeserializeObject<T>(jsonString, settings);
        }

        private class PrivateSetterContractResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty prop = base.CreateProperty(member, memberSerialization);

                if (!prop.Writable)
                {
                    var property = member as PropertyInfo;
                    if (property != null)
                    {
                        bool hasPrivateSetter = property.GetSetMethod(true) != null;
                        prop.Writable = hasPrivateSetter;
                    }
                }

                return prop;
            }
        }

/*
 * for private key i guess
        public static T FromJSON<T>(string url, string publicAuthKey, string privateAuthKey) where T : class
        {
            var req = WebRequest.Create(url) as HttpWebRequest;
            DateTime date = DateTime.Now.ToUniversalTime();
            req.Date = date;

            string stringToSign =
                req.Method + "\n"
                + date.ToString("r") + "\n"
                + req.RequestUri.AbsolutePath + "\n";

            byte[] buffer = Encoding.UTF8.GetBytes(stringToSign);

            var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(privateAuthKey));

            string signature = Convert.ToBase64String(hmac.ComputeHash(buffer));

            req.Headers[HttpRequestHeader.Authorization]
                = "BNET " + publicAuthKey + ":" + signature;

            return FromJSON<T>(req);
        }*/
    }
}