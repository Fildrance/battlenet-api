﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BattleNetAPI.Model.Achivement;
using BattleNetAPI.Model.Challange;
using BattleNetAPI.Model.Character;
using BattleNetAPI.Model.Guild;
using BattleNetAPI.Model.Infrastructure;
using BattleNetAPI.Model.Item;
using BattleNetAPI.Utilities;

namespace BattleNetAPI
{
    public class WowBattleNetApiClient : IWoWBattleNetApiClient
    {
        public WowBattleNetApiClient(Region region, Locale locale, string apiKey)
        {
            _region = region;
            _locale = locale;
            _apiKey = apiKey;

            switch (_region)
            {
                case Region.EU:
                    _host = "https://eu.api.battle.net";
                    break;
                case Region.KR:
                    _host = "https://kr.api.battle.net";
                    break;
                case Region.TW:
                    _host = "https://tw.api.battle.net";
                    break;
                case Region.CN:
                    _host = "https://www.battlenet.com.cn";
                    break;
                case Region.US:
                default:
                    _host = "https://us.api.battle.net";
                    break;
            }
        }

        #region Character

        public Task<Character> GetCharacterAsync(string realm, string name)
        {
            return GetCharacterAsync(realm, name, CharacterOptions.None);
        }

        public Task<Character> GetCharacterAsync(string realm, string name,
            CharacterOptions characterOptions)
        {
            return GetData<Character>(
                string.Format(@"{0}/wow/character/{1}/{2}?locale={3}{4}&apikey={5}", _host, realm, name, _locale,
                    CharacterUtility.buildOptionalQuery(characterOptions), _apiKey));
        }

        #endregion

        #region Guild

        public Task<Guild> GetGuildAsync(string realm, string name)
        {
            return GetGuildAsync(realm, name, GuildOptions.None);
        }

        public Task<Guild> GetGuildAsync(string realm, string name, GuildOptions realmOptions)
        {
            return GetData<Guild>(
                string.Format(@"{0}/wow/guild/{1}/{2}?locale={3}{4}&apikey={5}", _host, realm, name, _locale,
                    GuildUtility.buildOptionalQuery(realmOptions), _apiKey));
        }

        #endregion

        #region Realms

        public async Task<IEnumerable<Realm>> GetRealmsAsync()
        {
            RealmsData realmsData = await GetData<RealmsData>(
                string.Format(@"{0}/wow/realm/status?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return realmsData.Realms;
        }

        #endregion

        #region Items

        public Task<Item> GetItemAsync(int id)
        {
            return GetData<Item>(
                string.Format(@"{0}/wow/item/{1}?locale={2}&apikey={3}", _host, id, _locale, _apiKey));
        }

        public async Task<IEnumerable<ItemClassInfo>> GetItemClassesAsync()
        {
            ItemClassData itemClassData = await GetData<ItemClassData>(
                string.Format(@"{0}/wow/data/item/classes?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return itemClassData.Classes;
        }

        #endregion

        #region CharacterRaceInfo

        public async Task<IEnumerable<CharacterRaceInfo>> GetCharacterRacesAsync()
        {
            CharacterRacesData characterRacesData = await GetData<CharacterRacesData>(
                string.Format(@"{0}/wow/data/character/races?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return characterRacesData.Races;
        }

        #endregion

        #region CharacterClassInfo

        public async Task<IEnumerable<CharacterClassInfo>> GetCharacterClassesAsync()
        {
            CharacterClassesData characterClassesData = await GetData<CharacterClassesData>(
                string.Format(@"{0}/wow/data/character/classes?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return characterClassesData.Classes;
        }

        #endregion

        #region GuildRewardInfo

        public async Task<IEnumerable<GuildRewardInfo>> GetGuildRewardsAsync()
        {
            GuildRewardsData guildRewardData = await GetData<GuildRewardsData>(
                string.Format(@"{0}/wow/data/guild/rewards?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return guildRewardData.Rewards;
        }

        #endregion

        #region GuildPerkInfo

        public async Task<IEnumerable<GuildPerkInfo>> GetGuildPerksAsync()
        {
            GuildPerksData guildPerksData = await GetData<GuildPerksData>(
                string.Format(@"{0}/wow/data/guild/perks?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return guildPerksData.Perks;
        }

        #endregion

        #region Achievements

        public Task<AchievementInfo> GetAchievementAsync(int id)
        {
            return GetData<AchievementInfo>(
                string.Format(@"{0}/wow/achievement/{1}?locale={2}&apikey={3}", _host, id, _locale, _apiKey));
        }

        public async Task<IEnumerable<AchievementList>> GetAchievementsAsync()
        {
            AchievementData achievementLists = await GetData<AchievementData>(
                string.Format(@"{0}/wow/data/character/achievements?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return achievementLists.Lists;
        }

        public async Task<IEnumerable<AchievementList>> GetGuildAchievementsAsync()
        {
            AchievementData guildAchievementsList = await GetData<AchievementData>(
                string.Format(@"{0}/wow/data/guild/achievements?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return guildAchievementsList.Lists;
        }

        #endregion

        #region Battlegroups

        public async Task<IEnumerable<BattlegroupInfo>> GetBattlegroupsDataAsync()
        {
            BattlegroupData battlegroupInfo = await GetData<BattlegroupData>(
                string.Format(@"{0}/wow/data/battlegroups/?locale={1}&apikey={2}", _host, _locale, _apiKey));
            return battlegroupInfo.Battlegroups;
        }

        #endregion

        #region Challenges

        public Task<Challenges> GetChallengesAsync(string realm)
        {
            return GetData<Challenges>(
                string.Format(@"{0}/wow/challenge/{1}?locale={2}&apikey={3}", _host, realm, _locale, _apiKey));
        }

        #endregion

        #region fields

        private readonly string _apiKey;
        private readonly string _host;
        private readonly Locale _locale;
        private readonly Region _region;

        #endregion

        private async Task<T> GetData<T>(string url) where T : class
        {
            string json = await HttpUtility.GetResponceAsync(url);
            return JsonUtility.GetFromJson<T>(json);
        }
    }
}